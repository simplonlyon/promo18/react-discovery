import { useEffect, useState } from "react"
import RegisterForm from "./RegisterForm";
import { getAll } from "./user-service";


export default function UserList() {

    const [list, setList] = useState([]);

    async function getList() {
        const data = await getAll();
        setList(data);
    }

    function handleRegister(user) {
        setList([
            ...list,
            user
        ])
    }
    
    useEffect(() => {
        getList();
    }, []);


    return (
        <div>

            <h1>User List</h1>

            {list.map(item =>
                <div key={item.id}>
                    {item.email}, {item.role}
                </div>
            )}

            <RegisterForm onRegister={handleRegister} />
        </div>
    )
}