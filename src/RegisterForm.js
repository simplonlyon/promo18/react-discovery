import { useState } from "react";
import { postUser } from "./user-service";



export default function RegisterForm({onRegister}) {
    const [user, setUser] = useState({
        email: '',
        password: ''
    });

    const handleChange = (event) => {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }

    const register = async (event) => {
        event.preventDefault();
        const data = await postUser(user);
        onRegister(data);
    } 


    return (

        <form onSubmit={register}>
            <label>Email :</label>
            <input type="email" name="email" value={user.email} onChange={handleChange} />

            <label>Password :</label>
            <input type="password" name="password" value={user.password} onChange={handleChange} />

            <button>Register</button>
        </form>
    );
}