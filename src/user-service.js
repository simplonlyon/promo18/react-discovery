import axios from "axios";



export async function getAll() {
    const response = await axios.get('http://localhost:8000/api/user');
    return response.data;
}

export async function postUser(user) {
    const response = await axios.post('http://localhost:8000/api/user', user);
    return response.data;
}