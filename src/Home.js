import { useState } from "react";

function Home() {

    const [message, setMessage] = useState('Coucou');
    const [tab, setTab] = useState([
      'ga', 'zo', 'bu', 'meu'
    ]);
  
    const changeMessage = () => {
      setMessage('autre chose');
      setTab([
        ...tab,
        'nouveau'
      ]);
    }
  
    /*
    //ça fait exactement pareil
    function changeMessage() {
      setMessage('autre chose');
    }
    */
  
  
    return (
      <div>
        <p>{message}</p>

  
        {message.length > 6 && <p>Le message fait plus 6 caractères</p>}
  
        <button onClick={changeMessage}>Click !</button>
  
        {tab.map((item, index) =>
          <p key={index}>{item}</p>
        )}
  
      </div>
    );
  }
  
  export default Home;
  