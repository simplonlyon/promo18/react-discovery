import { useState } from "react";



export default function First() {

    const [count, setCount] = useState(0);

    const increment = () => {
        setCount(count + 1)
    };
    const decrement = () => {
        setCount(count - 1)
    };

    return (
        <div>
            <h2>Counter</h2>

            <button onClick={increment}>+</button>
            {count}
            <button onClick={decrement}>-</button>
        </div>
    );
}